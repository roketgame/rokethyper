﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoketGame
{
	public class CorePlayerCont : CoreCont<CorePlayerCont>
	{
		public Camera Cam;
		public override void Init()
		{
			enabled = false;
			Cam = Camera.main;
			base.Init();
		}


		public override void StartGame()
		{
			base.StartGame();
			enabled = true;
		}

	}
}