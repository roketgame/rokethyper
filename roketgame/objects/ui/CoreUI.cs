﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RoketGame
{
    public enum SelectableStatus { ENABLED, HIGHLIGHT, SELECTED, DISABLED };
    public enum TouchEventTarget { SELF, PARENT };
    public class CoreUI : CoreObject
    {



        [HideInInspector] public string Id;
        public Graphic TouchTarget;
        [HideInInspector] public RoketGame.EventTouchUI EventTouch = new RoketGame.EventTouchUI();
        private SelectableStatus selectableStatus;

        public override void Init()
        {
            base.Init();
            //if (uiName.Length <= 0)
            //{
            //    Debug.LogError(GetType() + " -> UI Name not defined!");
            //}

            //gameObject.SetActive(false);
        }


        public override void Load()
        {
            Loaded();
        }
        public virtual void Open()
        {
            Opened();
        }
        public virtual void Close()
        {
            Closed();
        }
        protected override void Loaded()
        {
            GameStatus = GameStatus.LOADED;
        }
        protected virtual void Opened()
        {
            GameStatus = GameStatus.OPENED;
        }
        protected virtual void Closed()
        {
            GameStatus = GameStatus.CLOSED;
        }


        public virtual void SetSelectable(SelectableStatus _status)
        {

            selectableStatus = _status;
        }
        public virtual SelectableStatus GetSelectable()
        {

            return selectableStatus;
        }



        /* TOUCH EVENT */
        public virtual void OnTouch(RoketGame.TouchUI info)
        {
            EventTouch.Invoke(info);
        }

    }

}