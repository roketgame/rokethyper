﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RoketGame
{
    public class CoreObject : MonoBehaviour
    {
        [HideInInspector] public bool IsGameStarted;
        [HideInInspector] public GameStatusEvent EventGameStatus = new GameStatusEvent();
      
       





        public virtual void Init()
        {
            GameStatus = GameStatus.INIT;
        }

        public virtual void Load()
        {
            Loaded();
        }
      
        protected virtual void Loaded()
        {
            GameStatus = GameStatus.LOADED;
        }

        public virtual void StartGame()
        {
            GameStatus = GameStatus.STARTGAME;
            IsGameStarted = true;
        }
        //public virtual void StartLevel(int level)
        //{
        //    GameStatus = GameStatus.STARTLEVEL;
        //}
        //public virtual void EndLevel(int level)
        //{
        //    GameStatus = GameStatus.ENDLEVEL;
        //}
        public virtual void GameOver(int result)
        {
            GameStatus = GameStatus.GAMEOVER;
            IsGameStarted = false;
        }

        public virtual void Update()
        {
        }


  
        /* GameStatus : oyunun genel akisinin belirlendigi statelerdir. INIT, STARtGAme gibi */
        private GameStatus gameStatus;
        public GameStatus GameStatus
        {
            get { return gameStatus; }
            set { 
                gameStatus = value;
                EventGameStatus.Invoke(gameStatus);
                if (RoketGame.Config.Instance.DebugLogLevel > 1) Debug.Log("BaseObject(" + name + ") gameStatus : " + gameStatus.ToString());

            }
        }


    }
}
