﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AnimElement
{
    public Vector3 StartPos;
    public Vector3 EndPos;
    public float AnimTime = 1;
    public float DelayLoopTime = 0.2f;
    public bool IsStarted ;
}
public class Tutorial : CoreCont<Tutorial>
{
    public List<AnimElement> ListSteps = new List<AnimElement>();
    public class EventTutorialCompleted : UnityEvent { };
    public EventTutorialCompleted EventCompleted = new EventTutorialCompleted();
    public float TimeScale = 1f; //anim zamaninda time.scale degeri
    // public static bool IsEnable;
    public GameObject CursorPrefab;
    // public bool IsCompleted;
  
    private int currStep;
    public GameObject cursor;
    // private Vector3 moveToStart;

   public override void Init()
    {
      
        // IsCompleted = true;
        base.Init();

      
    }

    public override void StartGame()
    {
        base.StartGame();
           cursor = Instantiate(CursorPrefab, Vector3.zero, Quaternion.identity);
        cursor.transform.parent =  CoreUiCont.Instance.transform;
        cursor.SetActive(false);

    }
    public void Play(int _step)
    {
        currStep = _step;
        // IsCompleted = false;
        
        cursor.SetActive(true);
          if(_step < ListSteps.Count) playStep(_step);
          Time.timeScale = TimeScale;
    }
    public void Stop()
    {
        Time.timeScale = 1;
        // isStarted = false;
        if (ListSteps.Count-1>= currStep)
        {
            ListSteps[currStep].IsStarted = false;
        }
        LeanTween.cancel(cursor);
        cursor.SetActive(false);
    }

    public void SavePositions(int _step, Vector3 _startPos, Vector3 _endPos, float _time=1, float _delayLoop=0.2f)
    {
        AnimElement elem = new AnimElement();
        elem.AnimTime = _time;
        elem.StartPos = _startPos;
        elem.EndPos = _endPos;
        elem.DelayLoopTime = _delayLoop;
        
      
        if(_step >= ListSteps.Count)
        {
            ListSteps.Add(elem);
        }
        else
        {
            ListSteps[_step] = elem;
        }
        // else
        // {
        //     IsCompleted = true;
        //     EventCompleted.Invoke();
        //     StopTutorial();
        // }
    }

    private void playStep(int _step)
    {
        
        if(_step < ListSteps.Count)
        {
            ListSteps[_step].IsStarted = true;
              moveTo(ListSteps[_step]);
        }
           
    }
        
    // public void SetStep(int _step)
    // {
    //     if(_step < Points.Count)
    //     {

    //         if(_step == 0)
    //         {
    //             Char getRandomChar = CharController.Instance.GetChar(true);
    //             if(getRandomChar)
    //             {
    //                 //moveToStart = getRandomChar.transform;
    //                 moveToStart = Camera.main.WorldToScreenPoint( getRandomChar.transform.position);
    //                 //moveToStart.eulerAngles = Vector3.zero;
    //             }
              
    //         }
    //         else
    //         {
    //             moveToStart = Points[_step - 1].position;
    //         }
    //         currStep = _step;
    //         moveTo(Points[currStep].position, 1);
    //     }
    //     else
    //     {
    //         IsCompleted = true;
    //         EventCompleted.Invoke();
    //         StopTutorial();
    //     }
       
    // }
    // public void NextStep()
    // {
    //     SetStep(currStep + 1);
    // }

    private void moveTo(AnimElement _element)
    {
        
        LeanTween.cancel(cursor);
        //Cursor.transform.position = new Vector3(moveToStart.position.x, 0, moveToStart.position.z);
        cursor.transform.position = _element.StartPos;
        //Cursor.transform.rotation = moveToStart.rotation;

        LTDescr d = LeanTween.move(cursor, _element.EndPos, _element.AnimTime);
        //LeanTween.rotate(Cursor, _trans.eulerAngles, _time);

        if (d != null)
        {
            d.setIgnoreTimeScale(true);

            d.setOnComplete(moveToCompleted);
        }
    }

    private void moveToCompleted()
    {

        float delay = ListSteps[currStep].DelayLoopTime * Time.timeScale;
        if (ListSteps[currStep].IsStarted) StartCoroutine(moveToCompletedDelay(delay));

    }

    private IEnumerator moveToCompletedDelay(float time)
    {
        yield return new WaitForSeconds(time);
        moveTo(ListSteps[currStep]);
    }
}
