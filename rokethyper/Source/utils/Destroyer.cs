﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;




namespace RoketGame
{
    public class Destroyer : HyperSceneObj
    {
        /* sade bu tag(lar)a sahip olan coreSceneObject'ler destroy edilecek */
       public string[] Tags = new string[] { "WillDestroy" };
        /* DeSPawn = true ise object CoreSceneCont poolCont tarafindan tekrar kullanılmak icin saklanir (bknx : roketgame pooling system) */
        public bool DeSpawn = true;
        public float DelayOfDestroyItem = 2f;
        public bool IsDestroyOnContactEnter;
        public bool IsDestroyOnContactExit;
        /* collide olan sceneObj'in parenti varsa onu destroy eder */
        public bool DestroyParentObjectIfExist = true;





        public override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);


            if (IsDestroyOnContactEnter) removeFromScene(collision.gameObject);

            //Debug.Log("RemoveProduct Destroyer " + collision.gameObject.name);


        }

        public override void OnCollisionExit(Collision collision)
        {
            base.OnCollisionExit(collision);
            if (IsDestroyOnContactExit) removeFromScene(collision.gameObject);

        }

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            if (IsDestroyOnContactEnter) removeFromScene(other.gameObject);


        }

        public override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);
            if (IsDestroyOnContactExit) removeFromScene(other.gameObject);

        }

        private void removeFromScene(GameObject _go)
        {
            if (_go == null) return;
            if (!Tags.Contains(_go.tag)) return;
            Debug.Log("tag " + _go.tag);
            CoreSceneObject obj = _go.GetComponent<CoreSceneObject>();
            if (obj)
            {
                StartCoroutine(destroy(obj, DelayOfDestroyItem));
            }

         

        }

        private IEnumerator destroy(CoreSceneObject obj, float delay)
        {

                yield return new WaitForSeconds(delay);
            CoreSceneObject destroyedObject = obj;
            if (obj.transform.parent && DestroyParentObjectIfExist)
            {
                if (obj.transform.parent.GetComponent<CoreSceneObject>())
                    destroyedObject = obj.transform.parent.GetComponent<CoreSceneObject>();
            }
              

            if (DeSpawn)
                CoreSceneCont.Instance.DeSpawnItem(destroyedObject);
            else
                CoreSceneCont.Instance.DestroyItem(destroyedObject);
               
        }



    }
}
