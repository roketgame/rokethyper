﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace RoketGame
{

   



    [RequireComponent(typeof(HyperLevelCont))]
    [RequireComponent(typeof(Tutorial))]
    public class HyperGameCont : CoreGameCont
    {
        public int SimulateFPS = 0;
        public int DebugLevel = -1; //if DebugMode > 0
        //public bool EnableLevelReadFromPlayerPrefs = true;
        [HideInInspector] public HyperLevelCont Level;
        public override void Init()
        {
            #if UNITY_EDITOR
            if(SimulateFPS > 0)
            {
                QualitySettings.vSyncCount = 0; 
                Application.targetFrameRate = SimulateFPS;
            }
           
            #endif


            if(HyperCameraCont.Instance)  HyperCameraCont.Instance.Init();

            Level = GetComponent<HyperLevelCont>();
            Level.Init();
            //if(Config.Instance.DebugMode > 0 && DebugLevel > -1)
            if( DebugLevel > -1)
            {
                Level.CurrLevel = DebugLevel;
            }
            else
            {
                Level.RestoreLevel();
            }
            
            base.Init();
            Level.InitLevel();


        }


        /* CoreGameCont'de normalde burada oyunu baslatiyoruz. HyperGameCont ise burada pageGame'den start'in tetiklenmesini bekleyecek. */
        protected override void uiContOnHandler(GameStatus _status)
        {
            if (_status == GameStatus.LOADED)
            {
                ((HyperUICont)CoreUiCont.Instance).OpenPageGame().EventStart.AddListener(onStarCalledFromPageGame);
            }
             
        }

        /* pageGame'den gelen start, daha sonra levelCont'dan ayni isimli event'in donusunu bekliyoruz */
        protected virtual void onStarCalledFromPageGame()
        {
            LevelCont.Instance.EventStart.AddListener(onStarCalledFromLevelCont);
           Level.StartLevel();
        }

        protected virtual void onStarCalledFromLevelCont()
        {
            StartGame();
        }

        public override void StartGame()
        {
            base.StartGame();
           
            //CoreUiCont.Instance.GetPageByClass<HyperPageGame>().

          

        }

        //private void onStartGameClicked()
        //{
        //    StartLevel();
        //}

        //public virtual void StartLevel()
        //{
        //   if(!LevelCont.Instance.IsLevelStarted) Level.StartLevel(Level.CurrLevel);


        //}



        public override void GameOver(int result)
        {
            base.GameOver(result);

            ((HyperUICont)CoreUiCont.Instance).OpenPageGameOver(result);

            foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
            {
               
                    foreach (CoreObject obj in go.GetComponents<CoreObject>())
                    {
                         if (obj != this)
                         {
                            //Debug.Log("obj name " + obj.name);
                             obj.GameOver(result);
                         }
                      
                    }
                   


            }
        }








        public override void Update()
        {
            base.Update();

            //if (Input.GetKeyDown("q"))
            //LaserCont.Instance.StartLasers();
        }



        

    }

}
 