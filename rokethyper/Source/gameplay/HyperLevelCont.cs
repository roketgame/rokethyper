﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;



public class HyperLevelCont : CoreCont<HyperLevelCont>
{
   [HideInInspector] public EventStartAction EventStart = new EventStartAction();
    [HideInInspector] public int CurrLevel;
    /* LevelTotalTime -> toplam level time'i, 0 dan buyukse isleme alinir */
    public float LevelTotalTime = 0;
    /* GameOverWhenTimeExceed -> totalTime suresi (varsa) doldugunda game over olsun mu? */
    public bool GameOverWhenTimeExceed = true;
    private float levelTime;

    public override void Init()
    {
        base.Init();
    }

    public virtual void InitLevel()
    {
        //init score
        {
            //if (PlayerPrefs.HasKey("Score")) SetLevelScore(0);
        }
    }

    public override void StartGame()
    {
        base.StartGame();

    }



    public virtual void StartLevel()
    {
        EventStart.Invoke();
        // TinySauce.OnGameStarted(CurrLevel.ToString()); 
    }

    public void TryEndLevel()
    {
        EndLevel(GetLevelResult());

    }

    public virtual void EndLevel(int levelResult)
    {

        if (levelResult > 0)
        {
            //PreEndLevel();
            ((HyperGameCont)CoreGameCont.Instance).GameOver(levelResult);

            if(levelResult == 1) //fail
            {
                ((HyperUICont)CoreUiCont.Instance).GetPageGameOver().GetTextfield(((HyperUICont)CoreUiCont.Instance).GetPageGameOver().TextfieldLevelScore).gameObject.SetActive(false);
            }
            
        }
        // TinySauce.OnGameFinished(CurrLevel.ToString(), levelResult == 2, HyperConfig.Instance.Score);


    }
    public void FixedUpdate()
    {
        if (IsGameStarted)
        {

            if(LevelTotalTime > 0)
            {
                if (LevelTime > LevelTotalTime)
                {
                    timeExceeded();
                }

                //if(CoreUiCont.Instance.GetPageByClass<HyperPageGame>())
                if(((HyperUICont) CoreUiCont.Instance).GetPageGame() )
                {
                    float ratio = (float)LevelTime / (float)LevelTotalTime;
                    ((HyperUICont)CoreUiCont.Instance).GetPageGame().SetBarScale("LevelTime", ratio);
                }
            }


                  

            LevelTime += Time.fixedDeltaTime;
        }

    }





    //public void AddScore(float _addScore)
    //{
    //    SetScore(GetScore() + _addScore);
    //}

    public void SetLevelScore(float _levelScore)
    {
       

        ((HyperUICont)CoreUiCont.Instance).GetPageGameOver().ViewLevelScore(_levelScore.ToString());
        HyperConfig.Instance.Score += _levelScore;
    }
    //public float GetLevelScore()
    //{
    //    return HyperConfig.Instance.Score;
       
    //}


    public virtual int GetLevelResult()
    {
        return -1;
    }



    public float LevelTime
    {
        get { return levelTime; }
        set { levelTime = value; }
    }




    private void timeExceeded()
    {
        if (GameOverWhenTimeExceed)
           TryEndLevel();
     
    }
    public void NextLevel()
    {
        SaveLevel();
        HyperGameCont.Instance.RestartGame();
    }
    public void RestoreLevel()
    {
        Debug.Log("RestoreLevel ");
        int level = 1;
        if (PlayerPrefs.HasKey("Level"))
        {
            level = PlayerPrefs.GetInt("Level");

       

        }
        CurrLevel = level;
    }


    public void SaveLevel()
    {
        PlayerPrefs.SetInt("Level", CurrLevel + 1);
        PlayerPrefs.Save();
    }


    public void SetLevel(int newLevel)
    {
        PlayerPrefs.SetInt("Level", newLevel);
        PlayerPrefs.Save();
        
        Invoke("restartAfterSetLevel", 0.2f);
    }
    private void restartAfterSetLevel()
    {
        HyperGameCont.Instance.RestartGame();

    }





}
