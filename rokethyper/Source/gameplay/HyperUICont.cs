﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace RoketGame
{
  


    public class HyperUICont : CoreUiCont
    {
        public bool IsSkipIntro = false;
        public override void Init()
        {
            base.Init();

           
          

        }
        public override void Load()
        {
            if (!IsSkipIntro)
            {
                OpenPageInit();


            }
            else
            {
                onIntroPageStart();
            }

        }

        private void onIntroPageStart()
        {
            Loaded();
        }
       

        public override void Update()
        {
            base.Update();

        }

        public HyperPageIntro OpenPageInit()
        {
            HyperPageIntro intro = transform.GetComponentInChildren<HyperPageIntro>(true);
            if (intro)
            {
                CoreUiCont.Instance.OpenPage(intro.GetElementId());
                intro.EventIntroLoaded.AddListener(onIntroPageStart);
            }
            return intro;
        }

        public HyperPageGame OpenPageGame()
        {
            HyperPageGame page = GetPageGame();
            if (page)
            {
                CoreUiCont.Instance.OpenPage(page.GetElementId());
            }
            return page;
        }
        public void OpenPageGameOver(int gameOverResult)
        {
            HyperPageGameOver page = transform.GetComponentInChildren<HyperPageGameOver>(true);
            if (page)
            {
                CoreUiCont.Instance.OpenPage(page.GetElementId());
                page.SetGameResult(gameOverResult);
            }
        }


        public HyperPageGame GetPageGame()
        {
            return transform.GetComponentInChildren<HyperPageGame>(true);
        }
        public HyperPageGameOver GetPageGameOver()
        {
            return transform.GetComponentInChildren<HyperPageGameOver>(true);
        }


    }

}