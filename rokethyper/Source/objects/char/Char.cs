﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RoketGame
{
    public enum CharAnimStatus { IDLE, WALKING, RUNNING, JUMP };

    [RequireComponent(typeof(Animator))]
    public class Char : HyperSceneObj
    {
        protected CharAnimStatus animStatus;
        public  Animator Anim;
        private float speed;
        private Vector3 lastPos;

        public override void Init()
        {
           base.Init();
           if(Anim == null)  Anim = GetComponent<Animator>();
        }

        public override void StartGame()
        {
            base.StartGame();

            PlayAnim(CharAnimStatus.IDLE);
        }

        public override void Update()
        {
            base.Update();

            if (GameStatus != GameStatus.STARTGAME) return;
            {//speed
                speed = (transform.position - lastPos).magnitude;
                lastPos = transform.position;
            }

            if(Anim)
            {//update anim
                Anim.SetFloat("speed", GetSpeed());
            }

        }
        public void PlayAnim(CharAnimStatus _animStatus)
        {
            animStatus = _animStatus;
            PlayAnim(_animStatus.ToString());
        }
        public void PlayAnim(string _anim)
        {
            Anim.Play(_anim);
        }
        public float GetSpeed()
        {
            return speed;
        }

    }
}