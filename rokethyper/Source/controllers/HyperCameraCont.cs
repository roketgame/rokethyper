﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;


[Serializable]

public struct CamProperty
{
    public string Id;
    public Vector3 LocalPos;
    public Vector3 LocalRot;
}
public class HyperCameraCont : CoreCont<HyperCameraCont>
{
    private Camera Camera;
    private List<CamProperty> listItems;
    private string currState;
    public override void Init()
    {
        base.Init();


        //foreach (CameraTransform elem in CamTrans)
        //{
        //    if (elem.Cam)
        //    {
        //        camTrans.Add(elem.Id, elem.Cam.transform);
        //        elem.Cam.gameObject.SetActive(false);
        //    }

        //}



        listItems = new List<CamProperty>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform item = transform.GetChild(i);
            if (item)
            {
                CamProperty p;
                p.Id = item.name.ToLower();
                p.LocalPos = item.transform.localPosition;
                p.LocalRot = item.transform.localEulerAngles;

                listItems.Add(p);

                if (i > 0) item.gameObject.SetActive(false);
            }
        }

        if (GetComponentsInChildren<Camera>().Length > 0)
        {
            Camera = GetComponentsInChildren<Camera>()[0];
            //if (listItems.Count > 0)
            {
                //Camera.transform.localPosition = listItems[0].Cam.transform.localPosition;
                //Camera.transform.localRotation = listItems[0].Cam.transform.localRotation;
                AnimTo(Camera.name, 0.1f);

            }



        }



    }


    public override void Update()
    {
        base.Update();

        //if(isRotateAroundAnim)
        //{
        //    Vector3 rot = transform.eulerAngles;
        //    rot.y -= 0.2f;
        //    transform.eulerAngles = rot;
        //}
    }

    public void AnimTo(string stateID, float animTime)
    {
       

        if(getItem(stateID.ToLower()).Id != null)
        {
            currState = stateID.ToLower();
            setTransition(getItem(stateID.ToLower()), animTime);
        }
          
       
    }

    public void AnimFrom(string stateID, float animTime)
    {


        if (getItem(stateID.ToLower()).Id != null)
        {
            CamProperty from = getItem(stateID.ToLower());
            Camera.transform.localPosition = from.LocalPos;
            Camera.transform.eulerAngles = from.LocalRot;
            setTransition(getItem(currState.ToLower()), animTime);
        }


    }

    private void setTransition(CamProperty property, float _time)
    {

        LeanTween.cancel(Camera.gameObject);
        //LeanTween.rotateLocal(gameObject, Vector3.zero, _time);
        LeanTween.moveLocal(Camera.gameObject, property.LocalPos, _time);
        LTDescr d = LeanTween.rotateLocal(Camera.gameObject, property.LocalRot, _time);
        //LTDescr d = LeanTween.descr(id);

        if (d != null)
        {

            d.setOnComplete(transitionOnCompleted);
        }
    }

    private void transitionOnCompleted()
    {
        //Debug.Log("oncomplete -> " + currStatus);
        
    }

    private CamProperty getItem(string id)
    {
        CamProperty p = listItems.Find(e => e.Id == id);
       return p;
    }
}
